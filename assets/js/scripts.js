(function ($) {
    'use strict';

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 75
    });

    const loginButtonEl = $('#login')
    loginButtonEl.on('click', function (){
        $('.card-login').attr('hidden', false)
        loginButtonEl.attr('hidden', true)
    })


    // show/hide password
    $('#passwordIcon').on('click', function() {
        if ($('input#password').attr('type') === 'text') {
            $('input#password').attr('type', 'password');
            $('#passwordIcon i').addClass( 'fa-eye-slash' );
            $('#passwordIcon i').removeClass( 'fa-eye' );
        }
        else if ($('input#password').attr('type') === 'password') {
            $('input#password').attr('type', 'text');
            $('#passwordIcon i').removeClass( 'fa-eye-slash' );
            $('#passwordIcon i').addClass( 'fa-eye' );
        }
    });

})(jQuery);
